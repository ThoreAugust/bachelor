const { print } = require('@tensorflow/tfjs');
const tf = require('@tensorflow/tfjs');
require('@tensorflow/tfjs-node');

const keys = [
    'Wrist',
    'Thumb_CMC', 'Thumb_MCP', 'Thumb_IP', 'Thumb_TIP',
    'Index_MCP', 'Index_PIP', 'Index_DIP', 'Index_TIP',
    'MiddleFinger_MCP', 'MiddleFinger_PIP', 'MiddleFinger_DIP', 'MiddleFinger_TIP',
    'RingFinger_MCP', 'RingFinger_PIP', 'RingFinger_DIP', 'RingFinger_TIP',
    'Pinky_MCP', 'Pinky_PIP', 'Pinky_DIP', 'Pinky_TIP',
  ];

  //convert/setup our data
const createTensors = (data) => {
    tf.util.shuffle(data);
    const inputs = data.map(item => {
        let result = [];
        let arr = Object.values(item).splice(1);
        let max = Math.max(...arr);
        let min = Math.min(...arr);
        const minMax = x =>{
            return ((x-min)/max-min);
        }
        keys.forEach(key => {
            result.push(minMax(Number.parseFloat(item[`${key}_x`])));
            result.push(minMax(Number.parseFloat(item[`${key}_y`])));
            result.push(minMax(Number.parseFloat(item[`${key}_z`])));
        })
        return result;
    });

    const outputs = data.map(item => [
        Number.parseInt(item["Label"]) === 0 ? 1 : 0,
        Number.parseInt(item["Label"]) === 1 ? 1 : 0,
        Number.parseInt(item["Label"]) === 2 ? 1 : 0,
        Number.parseInt(item["Label"]) === 3 ? 1 : 0,
        Number.parseInt(item["Label"]) === 4 ? 1 : 0,
        Number.parseInt(item["Label"]) === 5 ? 1 : 0,
        Number.parseInt(item["Label"]) === 6 ? 1 : 0,
        Number.parseInt(item["Label"]) === 7 ? 1 : 0,
        Number.parseInt(item["Label"]) === 8 ? 1 : 0,
        Number.parseInt(item["Label"]) === 9 ? 1 : 0,
        Number.parseInt(item["Label"]) === 10 ? 1 : 0,
        Number.parseInt(item["Label"]) === 11 ? 1 : 0,
        Number.parseInt(item["Label"]) === 12 ? 1 : 0,
        Number.parseInt(item["Label"]) === 13 ? 1 : 0,
        Number.parseInt(item["Label"]) === 14 ? 1 : 0,
        Number.parseInt(item["Label"]) === 15 ? 1 : 0,
        Number.parseInt(item["Label"]) === 16 ? 1 : 0,
        Number.parseInt(item["Label"]) === 17 ? 1 : 0,
        Number.parseInt(item["Label"]) === 18 ? 1 : 0,
        Number.parseInt(item["Label"]) === 19 ? 1 : 0,
        Number.parseInt(item["Label"]) === 20 ? 1 : 0,
        Number.parseInt(item["Label"]) === 21 ? 1 : 0,
        Number.parseInt(item["Label"]) === 22 ? 1 : 0,
        Number.parseInt(item["Label"]) === 23 ? 1 : 0,
        Number.parseInt(item["Label"]) === 24 ? 1 : 0,
        Number.parseInt(item["Label"]) === 25 ? 1 : 0,
        Number.parseInt(item["Label"]) === 26 ? 1 : 0,
        Number.parseInt(item["Label"]) === 27 ? 1 : 0,
        Number.parseInt(item["Label"]) === 28 ? 1 : 0,
        Number.parseInt(item["Label"]) === 29 ? 1 : 0,
    ]);

    const inputTensor = tf.tensor2d(inputs, [inputs.length, 63]);
    const outputTensor = tf.tensor2d(outputs, [outputs.length, 30]);
    

    return {
        input: inputTensor,
        output: outputTensor
    }
}
//build neural network

const getModel = async () =>{
    let model;
    try {
        model = await tf.loadLayersModel('file://./model/model.json');
    } catch (error) {
        console.error(error);
    }
    if (model) {
        return {model: model, trained: true};
    }
    model = await createModel();
    return {model: model, trained: false};
}

const createModel = async () => {
    let model = tf.sequential();
    model.add(tf.layers.dense({units: 76, activation: 'selu', inputShape: [63]}));
    model.add(tf.layers.dense({units: 76, activation: 'softsign'}));
    model.add(tf.layers.dense({units: 46, activation: 'sigmoid'}));
    model.add(tf.layers.dense({units: 30, activation: 'softmax'}));

    return model;
}

//train our network
const train =  async (data, epochs, batchSize) => {
    console.log('Creating Tensors!')
    const {input, output} = createTensors(data)
    console.log('Tensors created!')

    console.log('Getting model!');
    let {model, trained }= await getModel();
    console.log('Model gotten!');
    if (!trained) {
        model.compile({loss: tf.losses.logLoss,
            optimizer: tf.train.adam(), 
            metrics: ['accuracy']});
        console.log('Start training!');
        console.time();
        let history = await model.fit(input, output, {
            batchSize: batchSize,
            epochs: epochs,
            shuffle: true
        })
        console.timeEnd();
        console.log('End training!')
        trained = history ? true : false; 
        model.save('file://./model');
    }
    return trained;
}

//evaluate our network
const evaluate = async (data) => {
    //create Evaluation-Tensors
    const {input, output} = createTensors(data);

    //Get trained model
    let {model} = await getModel();
    model.compile({loss: tf.losses.logLoss,
        optimizer: tf.train.adam(), 
        metrics: ['accuracy']});
    let result = await model.evaluate(input, output);
    let loss = result[0].dataSync()[0];
    let acc = result[1].dataSync()[0];
    
    return {acc, loss};
}

const predict = async (data) => {
    const {input} = createTensors(data);
    let {model} = await getModel();
    let result = await model.predict(input);
    let scores = result.arraySync();
    let predictions = [];
    scores.forEach((element) => {
        let maxProb = {value: 0 , index: 0};
        element.forEach((element, index) =>{
            maxProb= maxProb.value < element ? {value: element, index:index} : maxProb;  
        });
        predictions.push(maxProb);
    });
    return predictions;
}

exports.evaluate = evaluate;
exports.train = train;
exports.predict = predict;