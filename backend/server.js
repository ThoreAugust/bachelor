const express = require('express');
const cors = require('cors');
const parser = require('body-parser');
const handTrain = require('./jsonFiles/hand-train.json');
const handTest = require('./jsonFiles/hand-test.json');
const handCollection = require('./jsonFiles/hand-collection.json');
const fs = require('fs');
const { train, evaluate, predict} = require('./machine');

const server = express();
const port = 8080;
const epochs = 250;
const batchSize = 8;
let done = false;
const errorMsg = 'Something went wrong!';
const successMsg = 'Ok!';

server.use(cors());
server.use(parser.json({limit: '5mb'}));
server.use(parser.urlencoded({extended: true}));

server.post('/hands', (req, res) => {
    try { 
        const imgData = req.body.image;
        const binaryData = Buffer.from(imgData, 'base64').toString('binary');
        fs.writeFile(`./images/${handCollection.length}.png`, binaryData, "binary", err => err ? console.log(err) : console.log('No Errors!'));
        delete req.body.image;
        handCollection.push(req.body);
        console.log(handCollection.length);
        fs.writeFileSync('./jsonFiles/hand-collection.json', JSON.stringify(handCollection, null, 2));
        res.json({msg: 'Thank you, for your contribution.'});
    } catch (error) {
        res.json({msg: errorMsg})
    }
    return;
});

server.post('/retrain', async (req, res) =>{
    done = false;
    fs.unlink('./model/model.json', err => {
        err ? console.log(err) : console.log('model.json deleted');
    });
    fs.unlink('./model/weights.bin', err => {
        err ? console.log(err) : console.log('weights.bin deleted');
    });
    done = await train (handTrain, epochs, batchSize);
    done ? res.send(successMsg) : res.send(errorMsg);
    return;
});

server.post('/addTrainingData', (req, res) => {
    const from = req.body.from;
    const to = req.body.to;
    for (let index = from; index <= to ; index++) {
        handTrain.push(handCollection[index]);
        fs.unlink(`./images/${index}.png`, err => {
            err ? console.log(err) : console.log(`./images/${index}.png deleted`);
        });
    }
    fs.writeFileSync('./jsonFiles/hand-train.json', JSON.stringify(handTrain, null, 2));
    console.log(handTrain.length);
    res.send(successMsg);
    return;
});

server.post('/eval', async (req, res) => {
    if(done){
        let {acc, loss} = await evaluate(handTest);
        res.status(200).send({msg: successMsg, accuracy: acc, loss: loss});
        return;
    }
    res.status(500).send(errorMsg)
    return;
})

server.post('/predict', async (req, res) => {
    const data = req.body;
    if(done){
        let result = await predict(data);
        res.status(200).send({msg: successMsg, predictions: result});
        return;
    }
    res.status(500).send(errorMsg);
    return;
})

server.listen(port, async () =>{
    console.log(`Server running at http://localhost:${port}`)
    console.log(handTrain.length);
    done = await train(handTrain, epochs, batchSize);
})