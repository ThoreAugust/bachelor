const videoElement = document.getElementsByClassName('input_video')[0];
const canvasElement = document.getElementsByClassName('output_canvas')[0];
const canvasCtx = canvasElement.getContext('2d');
const startbutton = document.getElementById('take_photo');
const photo = document.getElementById('photo');
const url = 'http://localhost:8080/hands';
let width = 1280;
let height;
let streaming = false;
const keys = [
  'Wrist',
  'Thumb_CMC', 'Thumb_MCP', 'Thumb_IP', 'Thumb_TIP',
  'Index_MCP', 'Index_PIP', 'Index_DIP', 'Index_TIP',
  'MiddleFinger_MCP', 'MiddleFinger_PIP', 'MiddleFinger_DIP', 'MiddleFinger_TIP',
  'RingFinger_MCP', 'RingFinger_PIP', 'RingFinger_DIP', 'RingFinger_TIP',
  'Pinky_MCP', 'Pinky_PIP', 'Pinky_DIP', 'Pinky_TIP',
]
const data = {};
    
const onResults = results => {
  if (results.multiHandLandmarks) {
    
    for (const landmarks of results.multiHandLandmarks) {
        landmarks.forEach((element, index) => {
          data[`${keys[index]}_x`] = element.x.toFixed(4);
          data[`${keys[index]}_y`] = element.y.toFixed(4);
          data[`${keys[index]}_z`] = element.z.toFixed(4);
            const row = document.getElementById(`position${index+1}`);
            const x = row.children[1];
            const y = row.children[2];
            const z = row.children[3];
            x.innerHTML = element.x.toFixed(4);
            y.innerHTML = element.y.toFixed(4);
            z.innerHTML = element.z.toFixed(4);
        });
    }
    /*fetch(url, {
      method: 'POST', // *GET, POST, PUT, DELETE, etc.
      mode: 'cors', // no-cors, *cors, same-origin
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data) // body data type must match "Content-Type" header
    });*/
  }
}

const hands = new Hands({locateFile: (file) => {
  return `https://cdn.jsdelivr.net/npm/@mediapipe/hands/${file}`;
}});
hands.setOptions({
  maxNumHands: 1,
  minDetectionConfidence: 0.75,
  minTrackingConfidence: 0.8
});
hands.onResults(onResults);

const camera = new Camera(videoElement, {
  onFrame: () => null,
  width: 1280,
  height: 720
});

const clearPhoto = () => {
  canvasCtx.fillStyle = "#AAA";
  canvasCtx.fillRect(0, 0, canvasElement.width, canvasElement.height);
  var data = canvasElement.toDataURL('image/png');
  photo.setAttribute('src', data);
}

const takePhoto = () =>{
    if (width && height) {
      canvasElement.width = width;
      canvasElement.height = height;
      canvasCtx.drawImage(videoElement, 0, 0, width, height);
    
      var data = canvasElement.toDataURL('image/png');
      photo.setAttribute('src', data);
    } else {
      clearphoto();
    }
  }

  const setLabel = () =>{
    data.Label = document.getElementById('label').value;
  }

  videoElement.addEventListener('canplay', function(ev){
  if (!streaming) {
    height = videoElement.videoHeight / (videoElement.videoWidth/width);
  
    if (isNaN(height)) {
      height = width / (4/3);
    }
  
    videoElement.setAttribute('width', width);
    videoElement.setAttribute('height', height);
    canvasElement.setAttribute('width', width);
    canvasElement.setAttribute('height', height);
    streaming = true;
  }
}, false);

startbutton.addEventListener('click', async function(ev){
  ev.preventDefault();
  setLabel();
  await takePhoto();
  hands.send({image: photo});
}, false);

camera.start();

clearPhoto();