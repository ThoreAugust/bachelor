const videoElement = document.getElementsByClassName('input_video')[0];
const videoCanvas = document.getElementsByClassName('video_canvas')[0];
const videoCanvasCtx = videoCanvas.getContext('2d');
const startbutton = document.getElementById('take_photo');
let labelInput;
const photo = document.getElementById('photo');
const url = 'https://bachelor.thoreaugust.de/api/hands'//'http://localhost:8080/hands';
let width = 1280;
let height;
let streaming = false;
const keys = [
  'Wrist',
  'Thumb_CMC', 'Thumb_MCP', 'Thumb_IP', 'Thumb_TIP',
  'Index_MCP', 'Index_PIP', 'Index_DIP', 'Index_TIP',
  'MiddleFinger_MCP', 'MiddleFinger_PIP', 'MiddleFinger_DIP', 'MiddleFinger_TIP',
  'RingFinger_MCP', 'RingFinger_PIP', 'RingFinger_DIP', 'RingFinger_TIP',
  'Pinky_MCP', 'Pinky_PIP', 'Pinky_DIP', 'Pinky_TIP',
]
const data = {};
    
const onImgResults = async results => {
  if (results.multiHandLandmarks) {
    document.getElementById('response').innerHTML = 'hand detected';
    for (const landmarks of results.multiHandLandmarks) {
        landmarks.forEach((element, index) => {
          data[`${keys[index]}_x`] = element.x.toFixed(4);
          data[`${keys[index]}_y`] = element.y.toFixed(4);
          data[`${keys[index]}_z`] = element.z.toFixed(4);
        });
    }
    let response = await fetch(url, {
      method: 'POST', // *GET, POST, PUT, DELETE, etc.
      mode: 'cors', // no-cors, *cors, same-origin
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data) // body data type must match "Content-Type" header
    });
    response = await response.json();
    document.getElementById('response').innerHTML = response.msg;
  }
}

const onVideoResults = results => {
  videoCanvasCtx.save();
  videoCanvasCtx.clearRect(0, 0, videoCanvas.width, videoCanvas.height);
  videoCanvasCtx.drawImage(
      results.image, 0, 0, videoCanvas.width, videoCanvas.height);
  if (results.multiHandLandmarks) {
    for (const landmarks of results.multiHandLandmarks) {
      drawConnectors(videoCanvasCtx, landmarks, HAND_CONNECTIONS,
                     {color: '#00FF00', lineWidth: 5});
      drawLandmarks(videoCanvasCtx, landmarks, {color: '#FF0000', lineWidth: 2});
    }
  }
  videoCanvasCtx.restore();
}

const handsForImg = new Hands({locateFile: (file) => {
  return `https://cdn.jsdelivr.net/npm/@mediapipe/hands/${file}`;
}});
handsForImg.setOptions({
  maxNumHands: 1,
  minDetectionConfidence: 0.75,
  minTrackingConfidence: 0.8
});
handsForImg.onResults(onImgResults);

const handsForVideo = new Hands({locateFile: (file) => {
  return `https://cdn.jsdelivr.net/npm/@mediapipe/hands/${file}`;
}});
handsForVideo.setOptions({
  maxNumHands: 1,
  minDetectionConfidence: 0.75,
  minTrackingConfidence: 0.8
});
handsForVideo.onResults(onVideoResults);

const camera = new Camera(videoElement, {
  onFrame: async () => {
    await handsForVideo.send({image: videoElement});
  },
  width: 1280,
  height: 720
});

const clearPhoto = () => {
  videoCanvasCtx.fillStyle = "#AAA";
  videoCanvasCtx.fillRect(0, 0, videoCanvas.width, videoCanvas.height);
  var data = videoCanvas.toDataURL('image/png');
  photo.setAttribute('src', data);
}

const takePhoto = () =>{
    if (width && height) {
      videoCanvas.width = width;
      videoCanvas.height = height;
      videoCanvasCtx.drawImage(videoElement, 0, 0, width, height);
    
      var imgData = videoCanvas.toDataURL();
      data.image = imgData.replace(/^data:image\/png;base64,/,"");
      photo.setAttribute('src', imgData);
    } else {
      clearphoto();
    }
  }

  const setLabel = () =>{
    document.getElementsByName('label').forEach(element => {
      if(element.checked){
        labelInput = element;
        return;
      } 
    });
    data.Label = labelInput.value;
  }

  videoElement.addEventListener('canplay', function(ev){
  if (!streaming) {
    height = videoElement.videoHeight / (videoElement.videoWidth/width);
  
    if (isNaN(height)) {
      height = width / (4/3);
    }
  
    videoElement.setAttribute('width', width);
    videoElement.setAttribute('height', height);
    videoCanvas.setAttribute('width', width);
    videoCanvas.setAttribute('height', height);
    streaming = true;
  }
}, false);

startbutton.addEventListener('click', async function(ev){
  ev.preventDefault();
  document.getElementById('response').innerHTML = '';
  setLabel();
  await takePhoto();
  handsForImg.send({image: photo});
}, false);

camera.start();

clearPhoto();