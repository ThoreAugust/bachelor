const videoElement = document.getElementsByClassName('input_video')[0];
const videoCanvas = document.getElementsByClassName('video_canvas')[0];
const videoCanvasCtx = videoCanvas.getContext('2d');
const startbutton = document.getElementById('take_photo');
let labelInput;
const photo = document.getElementById('photo');
const url = 'https://bachelor.thoreaugust.de/api/predict'//'http://localhost:8080/predict';
let width = 1280;
let height;
let streaming = false;
const keys = [
  'Wrist',
  'Thumb_CMC', 'Thumb_MCP', 'Thumb_IP', 'Thumb_TIP',
  'Index_MCP', 'Index_PIP', 'Index_DIP', 'Index_TIP',
  'MiddleFinger_MCP', 'MiddleFinger_PIP', 'MiddleFinger_DIP', 'MiddleFinger_TIP',
  'RingFinger_MCP', 'RingFinger_PIP', 'RingFinger_DIP', 'RingFinger_TIP',
  'Pinky_MCP', 'Pinky_PIP', 'Pinky_DIP', 'Pinky_TIP',
]
const uniCodes = ['񆄡', '񂇁','񂩡', '񅯡', '񀀁', '񀏁', '񅟁', '񃛁', '񃧁','񅊁','񅑡','񀟡','񀕁','񀘁','񀭁','񀼁','񁦁','񁧡','񁲁','񁼡','񃺡','񅢁','񅦡','񅮁','񂣡','񂱁','񂻡','񃇡','񄸁','񄵁']

let landmarksToPredict = [];
let timerStarted = false;

const onVideoResultsForVisualisation = results => {
  videoCanvasCtx.save();
  videoCanvasCtx.clearRect(0, 0, videoCanvas.width, videoCanvas.height);
  videoCanvasCtx.drawImage(
      results.image, 0, 0, videoCanvas.width, videoCanvas.height);
  videoCanvasCtx.restore();
  if (results.multiHandLandmarks) {
    startbutton.disabled = false;
  }
  if(results.multiHandLandmarks && timerStarted){
    let landmarkToPredict = {};
    for (const landmarks of results.multiHandLandmarks) {
        landmarks.forEach((element, index) => {
          landmarkToPredict[`${keys[index]}_x`] = element.x.toFixed(4);
          landmarkToPredict[`${keys[index]}_y`] = element.y.toFixed(4);
          landmarkToPredict[`${keys[index]}_z`] = element.z.toFixed(4);
        });
    }
    landmarksToPredict.push(landmarkToPredict);
  }
}

const handsForVisualisation = new Hands({locateFile: (file) => {
  return `https://cdn.jsdelivr.net/npm/@mediapipe/hands/${file}`;
}});
handsForVisualisation.setOptions({
  maxNumHands: 1,
  minDetectionConfidence: 0.75,
  minTrackingConfidence: 0.8
});
handsForVisualisation.onResults(onVideoResultsForVisualisation);

const camera = new Camera(videoElement, {
  onFrame: async () => {
    await handsForVisualisation.send({image: videoElement});
  },
  width: 1280,
  height: 720
});

const startTimer = async() =>{
  timerStarted = true;
  console.log('timer started')
  setTimeout( async ()=> {
    timerStarted = false;
    console.log('timer stopped');
    console.log(landmarksToPredict)
    await sendLandmarksToPredict();
  }, 5000)
}

const sendLandmarksToPredict = async () =>{
  let response = await fetch(url, {
    method: 'POST', // *GET, POST, PUT, DELETE, etc.
    mode: 'cors', // no-cors, *cors, same-origin
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(landmarksToPredict) // body data type must match 'Content-Type' header
  });
  response = await response.json();
  predSet = new Set();
  response.predictions.forEach(p => predSet.add(p.index));
  console.log(predSet)
  msg = '';
  predSet.forEach(i => msg += `${uniCodes[Number.parseInt(i)]},`)

  document.getElementById('response').innerHTML = msg;
}

videoElement.addEventListener('canplay', function(ev){
  if (!streaming) {
    height = videoElement.videoHeight / (videoElement.videoWidth/width);
  
    if (isNaN(height)) {
      height = width / (4/3);
    }
  
    videoElement.setAttribute('width', width);
    videoElement.setAttribute('height', height);
    videoCanvas.setAttribute('width', width);
    videoCanvas.setAttribute('height', height);
    streaming = true;
  }
}, false);

startbutton.addEventListener('click', async function(ev){
  ev.preventDefault();
  document.getElementById('response').innerHTML = '';
  landmarksToPredict = [];
  startTimer();
    
}, false);

camera.start();

startbutton.disabled = true;