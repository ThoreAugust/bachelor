# Bachelor

Steps
- [x] generate training csv for handforms 
- [x] train model to with generated csv
- [x] test model with handforms
- [x] predict handform label
- [x] collect more training data for better training results
- [x] retrain and evaluate the model
- [x] take label to return handformsymbol
- [x] display handform symbol

## Frontend
### justMediaPipe_Example

this was used to get an introduction to the MediaPipe Framework

### takePhoto

this will be used to Generate training and evaluation Data

### dataCollection

website for collecting more data for training.
User selects a hand phoneme he/she wants to submit, then does the hand phoneme and gets instant feedback if the hand was detected by the 'MediaPipe Framework'. Then records an image of the hand phoneme which then is converted to base64 and send to the server along with the detected anchor points. 

## Backend

### server.js  
Is a express-server that collects and saves training data, as well as accesses the evaluate, train and predict methods of machine.js.
On start the server either trains the model or if already trained loads it.

- `/hands` expects a json with the data and image [base64] of a hand phoneme and saves both into files.
- `/eval` calls evaluates the trained model with the test data and returns a json with the accuracy and loss of the model.
- `/predict` expects a json with the data for a hand phoneme and returns a prediction with the confidence and the predicted index.
- `/retrain` deletes the saved model and trains a new one and saves that.
- `/addTrainingData` expects a json with a [from: number] and a [to: number] then adds the collected data to the training data.

### machine.js

- `createTensor` converts data into Tensors for training or testing the model or for predicting a hand phoneme
- `getModel` either loads the already trained model from its file or calls `createModel`. Returns an object with the model and a boolean wether the model is trained or not.
- `createModel` creates a new sequential model with 5 layers and returns it
- `train` expects the data, the number of epochs and the batchSize with which it should train the model. It the call `createTensor` to convert the data, calls `getModel` to retrieve the model to train and then trains it with the given parameter. Returns a boolean wether the training was completed or not.
- `evaluate` expects the test data, then calls `createTensor` to convert the data and afterwards retrieves the model with `getModel` to then evaluate said model.
- `predict` expects the data with which to perform the prediction, converts it with `createTensors`, retrieves the model with `getModel` and makes a prediction based on the provided data. Returns an object with the highest confidence and the corresponding index.


